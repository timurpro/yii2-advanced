<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'cart' => 'orders/default/index',
            ]
        ],
        'assetManager' => [
            'linkAssets' => true,
            'converter' => [
                'class' => 'yii\web\AssetConverter',
                'commands' => [
                    'less' => ['css', 'export PATH=/Users/rtr/perl5/bin:/Users/rtr/perl5/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin; lessc {from} {to} --source-map -x'],
                ],
            ],
        ],
    ],
    'modules' => [
        'products' => [
            'class' => 'common\modules\Products\Module',
        ],
        'menu' => [
            'class' => 'common\modules\Menu\Module',
        ],
        'categories' => [
            'class' => 'common\modules\Categories\Module',
        ],
        'orders' => [
            'class' => 'common\modules\Orders\Module',
        ],
    ],
];
