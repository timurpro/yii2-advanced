<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class Menu extends ActiveFixture
{
    public $modelClass = 'common\modules\Menu\models\Menu';
}