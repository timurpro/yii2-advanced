<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class Category extends ActiveFixture
{
    public $modelClass = 'common\modules\Categories\models\Category';
}