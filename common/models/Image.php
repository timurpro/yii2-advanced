<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Image model
 *
 * @property integer $id
 * @property string $link
 * @property string $parent_class
 * @property ingeger $parent_id
 */
class Image extends ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%images}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['link', 'parent_class'],'string'],
            [['parent_class'], 'required'],
            [['parent_id'], 'integer'],
        ];
    }

    public function upload() 
    {
        $this->link = 'uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
        if ( $this->validate() ) {
            $this->imageFile->saveAs($this->link, false);
            return true;
        }
        return false;
    }

    public function getParent() 
    {
        return $this->hasOne($this->parent_class, ['id' => 'parent_id'])->where(['parent_class' => $this->parent_class]);
    }

}
