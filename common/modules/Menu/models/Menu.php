<?php

namespace common\modules\Menu\models;

use Yii;

/**
 * This is the model class for table "{{%menu}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $link
 * @property string $description
 * @property integer $parent_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'link'], 'required'],
            [['description'], 'string'],
            [['parent_id', 'created_at', 'updated_at'], 'integer'],
            [['link'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            /*'id' => Yii::t('app\Menu', 'ID'),
            'name' => Yii::t('app\Menu', 'Name'),
            'link' => Yii::t('app\Menu', 'Link'),
            'description' => Yii::t('app\Menu', 'Description'),
            'parent_id' => Yii::t('app\Menu', 'Parent ID'),
            'created_at' => Yii::t('app\Menu', 'Created At'),
            'updated_at' => Yii::t('app\Menu', 'Updated At'),*/
        ];
    }

}
