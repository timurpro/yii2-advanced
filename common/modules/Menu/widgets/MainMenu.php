<?php

namespace common\modules\Menu\widgets;

use Yii;
use common\modules\Menu\models\Menu;

class MainMenu extends \yii\base\Widget
{
    public function init() {
        $menuItems = Menu::find()->all();
        $itemsList = [];
        foreach ( $menuItems as $item ) {
            $itemsList[] = [
                'label' => $item->name,
                'url' => [$item->link],
            ];
        }
        if ( !empty($itemsList) ) {
            echo \yii\widgets\Menu::widget([
                'items' => $itemsList,
                'options' => [
                    'class' => 'menu-list',
                ],
                'linkTemplate' => '<div><a href="{url}">{label}</a></div>',
            ]);
        }
    }
}

?>