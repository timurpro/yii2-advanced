<?php

namespace common\modules\Products\widgets;

use Yii;
use yii\widgets\ListView;
use common\modules\Products\assets\ProductsAsset;
use yii\helpers\Url;

class ProductsListWidget extends ListView
{
    public $itemView = '@common/modules/Products/widgets/views/_product';
    public $layout = "{items}\n{pager}";

    public function init() {
        ProductsAsset::register($this->getView());
        parent::init();
    }
}

?>