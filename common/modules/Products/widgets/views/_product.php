<?php
use yii\helpers\Url;
?>
<div class="col-sm-4 col-md-4 col-xs-4">
    <div class="panel panel-default product_card" x-data-id="<?= $model->id ?>">
        <div class="panel-body">
            <div class="product_name"><a href="<?= Url::to(['/products/card/index', 'id' => $model->id]); ?>"><?= $model->name ?></a></div>
            <div class="product_image"></div>
            <div class="product_price"><?= $model->price ?></div>
            <div class="cart_button">В КОРЗИНУ</div>
        </div>
    </div>
</div>