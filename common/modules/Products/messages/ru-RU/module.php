<?php

return [
    'Product' => 'Продукт',
    'Products' => 'Продукты',
    'Product details' => 'Информация по продукту',
    'Products list' => 'Список продуктов',
    'Here you can manage your products' => 'На данной странице вы можете управлять своими продуктами',
    'Edit' => 'Править',
    'Back to list' => 'К списку',
    'Cancel' => 'Отмена',
    'Product create' => 'Создание продукта',
    'Product edit' => 'Редактирование продукта',
    'New product' => 'Новый продукт'
];

?>