<?php

return [
    // Fields
    'Id' => 'Ид',
    'Name' => 'Имя',
    'Price' => 'Цена',
    'Description' => 'Описание',
    'Article' => 'Артикул',
    'Created at' => 'Дата создания',
    'Updated at' => 'Дата изменения',
    'Status' => 'Статус',
    'Categories_ids' => 'Категории',
    'Categories' => 'Категории',
    'Images' => 'Изображения',

    // Status
    'Active' => 'Активный',
    'Deleted' => 'Удален',

    // Error messages
    'Failed to load images' => 'Не удалось загрузить изображения',
];

?>