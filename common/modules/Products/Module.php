<?php

namespace common\modules\Products;

use Yii;
use yii\helpers\Url;
/**
 * Products module definition class
 */
class Module extends \yii\base\Module
{
    use \common\traits\I18NModuleTrait;
    private static $I18N_NAMESPACE = 'common/modules/Products';
    private static $I18N_GROUPS = [ 'model', 'card', 'module' ];
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\Products\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }

    public static function menuElements() {
        return [
            'label' => self::t('module', 'Products'),
            'icon' => 'ti-package',
            'url' => Url::to(['/products/default/index']),
        ];
    }
}
