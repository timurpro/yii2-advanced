<?php

namespace common\modules\Products\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use common\modules\Products\Module;
use common\models\Image;
use common\traits\ImagesTrait;
use yii\helpers\ArrayHelper;

/**
 * Product model
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $article
 * @property double $price
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Product extends ActiveRecord {

    use ImagesTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => \voskobovich\linker\LinkerBehavior::className(),
                'relations' => [
                    'categories_ids' => [
                        'categories',
                        'updater' => [],
                    ],
                    'images_ids' => [
                        'images',
                        'updater' => [],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            ['price', 'double'],
            [['name', 'description', 'article'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['categories_ids'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('model', 'Id'),
            'name' => Module::t('model', 'Name'),
            'price' => Module::t('model', 'Price'),
            'description' => Module::t('model', 'Description'),
            'article' => Module::t('model', 'Article'),
            'created_at' => Module::t('model', 'Created at'),
            'updated_at' => Module::t('model', 'Updated at'),
            'status' => Module::t('model', 'Status'),
            'categories_ids' => Module::t('model', 'Categories'),
            'images' => Module::t('model', 'Images'),
        ];
    }

    public function getCategories()
    {
        return $this->hasMany(\common\modules\Categories\models\Category::className(), ['id' => 'category_id'])
             ->viaTable('{{%categories_products}}', ['product_id' => 'id']);
    }
    
    public function getCategoriesString()
    {
        $categories = $this->getCategories()->asArray()->all();
        return implode(' | ',ArrayHelper::map($categories, 'id', 'name'));
        
    }
    
    public static function getStatusesList() {
        return [
            self::STATUS_ACTIVE => Module::t('model', 'Active'),
            self::STATUS_DELETED => Module::t('model', 'Deleted')
        ];
    }

    public static function getStatusLabel( $status ) {
        $statuses = self::getStatusesList();
        return !empty($statuses[$status]) ? $statuses[$status] : '';
    }
    
}