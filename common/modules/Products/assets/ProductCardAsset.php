<?php

namespace common\modules\Products\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ProductCardAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/Products/assets';
    public $css = [
    ];
    public $js = [
        'js/productsCard.js'
    ];
    public $depends = [
        'frontend\assets\AppAsset'
    ];
}
