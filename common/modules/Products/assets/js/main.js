$(document).ready(function() {
    $('.product_card .cart_button').on('click', function(event) {
        event.preventDefault();
        var id = $(event.target).parents('.product_card').attr('x-data-id');
        addProductToCart(id);
    })
});