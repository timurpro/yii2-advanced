<?php

namespace common\modules\Products\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ProductsAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/Products/assets';
    public $css = [
    ];
    public $js = [
        'js/main.js'
    ];
    public $depends = [
        'frontend\assets\AppAsset'
    ];
}
