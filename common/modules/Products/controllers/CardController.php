<?php

namespace common\modules\Products\controllers;

use yii\web\Controller;
use common\modules\Products\models\Product;
use common\modules\Products\assets\ProductCardAsset;
use \yii\web\NotFoundHttpException;
use common\modules\Products\Module;

/**
 * Контроллер карточки товара
 */
class CardController extends Controller
{

    public function init() {
        ProductCardAsset::register($this->getView());
    }
    
    /**
     * Отображение карточки товара
     * @return string
     */
    public function actionIndex($id = null)
    {
        if ( !($product = Product::findOne($id)) ) {
            throw new NotFoundHttpException(Module::t('card', 'Product not found'));
        }
        return $this->render('index', [
            'product' => $product,
        ]);
    }
}
