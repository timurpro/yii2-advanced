<?php

namespace common\modules\Products\controllers\backend;

use yii\web\Controller;
use common\modules\Products\models\Product;
use common\modules\Products\Module;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

/**
 * Default controller for the `Products` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['view', 'edit', 'save', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function Init() {
        parent::Init();
        $this->setViewPath($this->getViewPath().'/backend');
    }

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'title' => 'Продукты'
        ]);
    }

    public function actionView($id = null) {
        if ( !($model = Product::findOne($id)) ) {
            \Yii::$app->getSession()->setFlash('error', 'Product not found');
            return $this->redirect(['index']);
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionEdit($id = null) {
        if ( !($model = Product::findOne($id)) ) {
            $model = new Product();
        }
        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionSave($id = null) {
        if( !($model = Product::findOne($id)) ) {
            $model = new Product();
        }
        if( $model->load(\Yii::$app->request->post()) && $model->save() ) {
            $model->setImages(UploadedFile::getInstances($model, 'images'));
            \Yii::$app->getSession()->setFlash('success', 'Save Ok');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            \Yii::$app->getSession()->setFlash('error', 'Save Error');
            return $this->render('edit', [
                'model' => $model,
            ]);
        }
    }

}
