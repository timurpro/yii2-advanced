<?php

use yii\helpers\Html;
use common\modules\Products\Module;

$this->title = $product->name;

?>
<div class="Products-card-index">
    <h1 class="name"><?= $product->name; ?></h1>
    <div class="article"><?= Module::t('card', 'Артикул'); ?>: <?= $product->article ?></div>
    <div class="pictures">
        <div class="current">
            <?php if ( !empty($product->images) ) { ?>
                <img src="/<?= $product->images[0]->link ?>"/>
            <?php } else { echo "?"; } ?>
        </div>
    </div>
    <div class="price_info">
        <div class="price">
            <?= Module::t('card', 'Цена'); ?>: <span class="num"><?= $product->price; ?> <?= Module::t('card', 'руб.'); ?></span>
        </div>
        <div class="presence">
            <?= Module::t('card', 'Наличие'); ?>: более 500 шт
        </div>
        <div class="rating">
            <?= Module::t('card', 'Оценка товара'); ?>: 25 тысяч звезд
        </div>
        <div class="buy">
            <?= Html::button(Module::t('card', 'Купить'), ['class' => 'btn btn-primary buy_button', 'x-product-id' => $product->id]); ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <h2 class="description_title"><?= Module::t('card', 'Description'); ?></h2>
    <div class="description">
        <?= $product->description ?>
    </div>
</div>