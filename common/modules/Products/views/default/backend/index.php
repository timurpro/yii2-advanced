<?php

/* @var $this yii\web\View */

$this->title = $title;

use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use common\modules\Products\models\Product;
use common\modules\Products\Module;

?>

<div class="Products-default-index">
    <section id="page-title">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="mainTitle"><?= Module::t('module', 'Products list'); ?></h1>
                <span class="mainDescription"><?= Module::t('module', 'Here you can manage your products'); ?></small></span>
            </div>
        </div>
    </section>
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <div class="col-md-12">
                <?= 
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            'id', 
                            [
                                'attribute' => 'name',
                                'value' => function($model) {
                                    return Html::a(Html::encode($model->name), Url::to(['view', 'id' => $model->id]));
                                },
                                'format' => 'raw',
                            ], 
                            //'description', 
                            'article', 
                            'price', 
                            [
                                'attribute' => 'name',
                                'value' => function($model) {
                                    return Product::getStatusLabel($model->status);
                                },
                            ],
                            'created_at:datetime', 
                            'updated_at:datetime'
                        ],
                        'layout' => "{items}\n{pager}",
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover',
                        ]
                    ]);
                ?>
            </div>
        </div>
        <?= Html::a('New', ['edit'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>


