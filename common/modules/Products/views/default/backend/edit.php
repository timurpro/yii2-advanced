<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\modules\Products\models\Product;
use common\modules\Categories\models\Category;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use common\modules\Products\Module;

?>
<div class="Products-default-edit">
    <section id="page-title">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="mainTitle"><?= Module::t('module', 'Product'); ?>: <?= !isset($model->name) ? Module::t('module', 'New product') : $model->name;  ?></h1>
                <span class="mainDescription"><?= !isset($model->name) ? Module::t('module', 'Product create') : Module::t('module', 'Product edit'); ?></small></span>
            </div>
        </div>
    </section>
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin([
                    'id' => 'ProductEditForm',
                    'method' => 'post',
                    'action' =>  ['save', 'id' => $model->id],
                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'article') ?>
                <?= $form->field($model, 'price') ?>
                <?=
                    $form->field($model, 'categories_ids')->dropDownList(
                        ArrayHelper::map(Category::find()->asArray()->all(), 'id', 'name'),
                        [
                            'multiple' => true,
                            'selected' => ArrayHelper::map($model->getCategories()->asArray()->all(), 'id', 'name')
                        ]
                    );
                ?>
                <?=
                    $form->field($model, 'status')->dropDownList(
                        Product::getStatusesList(), // items
                        [ //options
                            'selected' => $model->status
                        ]
                    );
                ?>
                <?= $form->field($model, 'images[]')->fileInput(['multiple' => true, 'accept' => 'image/*']); ?>
                <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                    'preset' => 'basic'
                ]); ?>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']); ?>
                <?= Html::a(Module::t('module', 'Cancel'), ['view', 'id' => $model->id], ['class' => 'btn']); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

