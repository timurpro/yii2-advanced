<?php

/* @var $this yii\web\View */

use yii\widgets\DetailView;
use yii\helpers\Html;
use common\modules\Products\models\Product;
use common\modules\Categories\models\Category;
use yii\helpers\ArrayHelper;
use common\modules\Products\Module;

?>

<div class="Products-default-view">
    <section id="page-title">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="mainTitle"><?= Module::t('module', 'Product'); ?>: <?= $model->name ?></h1>
                <span class="mainDescription"><?= Module::t('module', 'Product details'); ?></small></span>
            </div>
        </div>
    </section>
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <div class="col-md-12">
            <?= 
                DetailView::widget([
                    'model' => $model,
                    'attributes' => [                                         
                        'name',
                        'article',
                        'price',
                        [
                            'value' => $model->getCategoriesString(),
                            'attribute' => 'Categories',
                        ],
                        [ 
                            'value' => Product::getStatusLabel($model->status),
                            'attribute' => 'status',
                        ], 
                        'created_at:datetime', 
                        'updated_at:datetime',
                        'description:html'
                    ],
                ]);
            ?>
        </div>
    </div>
        <?= Html::a(Module::t('module', 'Edit'), ['edit', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
        <?= Html::a(Module::t('module', 'Back to list'), ['index'], ['class' => 'btn']); ?>
    </div>
</div>
    <?= \Yii::$app->session->getFlash('error'); ?>
