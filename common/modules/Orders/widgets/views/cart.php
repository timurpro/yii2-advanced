<?php 
use yii\helpers\Url;
use common\modules\Orders\Module as Orders;
?>

<div class="cart_body">
<a href="<?= Url::to(['/cart']); ?>">Корзина</a><br>
<?php if ( $cart->totalCount == 0 ) { ?>
    Корзина пуста
<?php } else { ?>
    <?= Orders::t('messages', 'Cart contains {count,plural,=0{no items} =1{# item} other{# items}}', ['count' => $cart->totalCount]); ?><br> 
    на сумму <?= $cart->totalPrice; ?> руб.
<?php } ?>
</div>