<?php

namespace common\modules\Orders\widgets;

use Yii;
use common\modules\Orders\models\Order;
use common\modules\Orders\Module as Orders;
use yii\helpers\Url;

class Cart extends \yii\base\Widget
{
    public $viewPath = 'common\modules\Orders\views\widgets';

    public function init() {
        Orders::registerTranslations();
        $cart = Orders::getCart();
        echo $this->render('cart', [
            'cart' => $cart
        ]);
    }
}

?>