<?php

return [
    'Cart contains {count,plural,=0{no items} =1{# item} other{# items}}' => '{count,plural,=0{Корзина пуста} one{Содержит # товар} few{Содержит # товара} many{Содержит # товаров} other{Содержит # товара}}',
];

?>