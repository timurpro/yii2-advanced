<?php

namespace common\modules\Orders\controllers;

use yii\web\Controller;
use common\modules\Orders\models\Order;
use common\modules\Orders\Module as Orders;
use common\modules\Products\models\Product;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Default controller for the `orders` module
 */
class ProductsController extends Controller
{
    /**
     * @todo: Написать комментарий к функции
     */
    public function actionAdd($id) {
        if ( empty($id) ) {
            return 'Ошибочка вышла';
        }
        $cart = Orders::getCart(true);
        // Ищем продкут по id
        $product = Product::findOne($id);
        if ( empty($product) ) {
            return 'Продукт не найден';
        }
        $cart->addProductToCart($product);
        return $this->renderAjax('_cart');
    }

    public function actionRemove($id) { 
        $cart = Orders::getCart();
        $product = $cart->getProducts()->where(['id' => $id])->one();
        if ( !empty($product) ) {
            $cart->unlink('products', $product, true);
        }
        return $this->redirect(['/cart']);
    }
}