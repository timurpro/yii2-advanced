<?php

namespace common\modules\Orders\controllers;

use yii\web\Controller;
use common\modules\Orders\models\Order;
use common\modules\Orders\Module as Orders;
use common\modules\Products\models\Product;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Default controller for the `orders` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $cart = Orders::getCart();
        $productsDataProvider = new ActiveDataProvider([
            'query' => $cart->getOrderProducts()
        ]);
        return $this->render('index', [
            'cart' => $cart,
            'productsDataProvider' => $productsDataProvider
        ]);
    }
}
