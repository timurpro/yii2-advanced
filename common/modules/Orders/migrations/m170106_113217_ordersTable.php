<?php

use yii\db\Migration;

class m170106_113217_ordersTable extends Migration
{
    /**
     * Название таблицы
     */
    const TABLE_NAME = "{{%orders}}";
    const PRODCUTS_REL_TABLE_NAME = "{{%orders_products}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'status' => $this->integer(),
            'comments' => $this->text(),
            /* Информация по доставке */
            'city' => $this->string(),
            'address' => $this->string(),
            'postal_code' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable(self::PRODCUTS_REL_TABLE_NAME, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'product_id' => $this->integer(),
            'count' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
        $this->dropTable(self::PRODCUTS_REL_TABLE_NAME);
    }

}
