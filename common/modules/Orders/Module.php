<?php

namespace common\modules\Orders;

use Yii;
use common\modules\Orders\models\Order;

/**
 * orders module definition class
 */
class Module extends \yii\base\Module
{
    const I18N_NAMESPACE = 'common/modules/Orders';
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\Orders\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();
        // custom initialization code goes here
    }

    public static function registerTranslations()
    {
        Yii::$app->i18n->translations[self::I18N_NAMESPACE.'/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@'.self::I18N_NAMESPACE.'/messages',
            'fileMap' => [
                self::I18N_NAMESPACE.'/messages' => 'messages.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t(self::I18N_NAMESPACE . '/' . $category, $message, $params, $language);
    }

    /**
     * Функция возвращает объект корзины
     *
     * Корзина - это объект Order в статусе Order::STATUS_CART
     * Функция проверяет наличие корзины в статусе Order::STATUS_CART и id == $_SESSION['cart_id'].
     * Если корзина не найдена, то пробует найти ее по идентификтору пользователя (user_id).
     * В противном случае - возвращает пустой объект Order.
     * При использовании параметра $saveIfNew = true, сохраняет новую корзину в базе.
     *
     * Использование:
     * $cart = \common\modules\Orders\Module::getCart();
     *
     * @param $saveIfNew boolean  Сохранить новую корзину, если не найдена
     * @return \common\modules\Orders\models\Order  объект корзины
     * @author Рахимжанов Тимур <t@timur.pro>
     **/
    public static function getCart( $saveIfNew = false )
    {
        $session = Yii::$app->getSession();
        $cart_id = $session->get('cart_id');
        if ( !($cart = Order::find()->where(['id' => $cart_id, 'status' => Order::STATUS_CART])->one() )) {
            if ( !Yii::$app->user->isGuest && !($cart = Order::find()->where(['user_id' => Yii::$app->user->id, 'status' => Order::STATUS_CART])->one())) {
                $cart = Yii::CreateObject(Order::ClassName());
                $cart->user_id = Yii::$app->user->id;
            }
        }
        if ( empty($cart) ) {
            $cart = Yii::CreateObject(Order::ClassName());
        }
        if ( $saveIfNew ) {
            $cart->save();
        }
        $session->set('cart_id', $cart->id);
        return $cart;
    }
}
