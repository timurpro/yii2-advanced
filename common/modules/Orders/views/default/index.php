<?php

use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\grid\ActionColumn;
use yii\helpers\Url;

$this->title = "Корзина";

?>
<div class="orders-default-index">
<?php
    echo GridView::widget([
        'dataProvider' => $productsDataProvider,
        'columns' => [
            ['class' => SerialColumn::className()],
            'product.name',
            'product.price',
            'count',
            [
                'class' => ActionColumn::ClassName(),
                'template' => '{delete}',
                'urlCreator' => function ($action, $model) {
                    return Url::to(['/orders/products/remove',  'id' => $model->product->id]);
                }
            ],
        ]
    ]);
?>
</div>
