<?php

namespace common\modules\Orders\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%orders}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $status
 * @property string $comments
 * @property string $city
 * @property string $address
 * @property integer $postal_code
 * @property integer $created_at
 * @property integer $updated_at
 */
class Order extends \yii\db\ActiveRecord
{
    /* СТФТУСЫ */
    const STATUS_CART = 10;
    const STATUS_NEW  = 20;
    const STATUS_POSTED = 30;
    const STATUS_ARCHIVED = 40;
    const STATUS_DELETED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'postal_code', 'created_at', 'updated_at', 'status'], 'integer'],
            [['comments'], 'string'],
            [['city', 'address'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_CART],
            ['status', 'in', 'range' => [
                self::STATUS_CART,
                self::STATUS_NEW,
                self::STATUS_POSTED,
                self::STATUS_ARCHIVED,
                self::STATUS_DELETED
            ]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            /*'id' => Yii::t('app\Orders', 'ID'),
            'user_id' => Yii::t('app\Orders', 'User ID'),
            'status' => Yii::t('app\Orders', 'Status'),
            'comments' => Yii::t('app\Orders', 'Comments'),
            'city' => Yii::t('app\Orders', 'City'),
            'address' => Yii::t('app\Orders', 'Address'),
            'postal_code' => Yii::t('app\Orders', 'Postal Code'),
            'created_at' => Yii::t('app\Orders', 'Created At'),
            'updated_at' => Yii::t('app\Orders', 'Updated At'),*/
        ];
    }

    public function getOrderProducts() {
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id'])->with('product');
    }

    public function getProducts() {
        return $this->hasMany(\common\modules\Products\models\Product::className(), ['id' => 'product_id'])->via('orderProducts');
    }

    public function isProductInOrder( $product ) {
        if ( empty($product) ) {
            return false;
        }
        $product_id = $product;
        if ( $product instanceof \yii\db\ActiveRecord ) {
            $product_id = $product->id;
        }
        foreach ( $this->orderProducts as $orderProduct ) { 
            if ( $orderProduct->product->id == $product_id ) { 
                return true;
            }
        }
        return false;
    }

    public function addProductToCart( $product ) {
        if ( empty($product) ) {
            return false;
        }
        if ( $this->isProductInOrder($product) ) {
            $orderProduct = $this->getOrderProducts()->where(['product_id' => $product->id])->one();
            $orderProduct->count++;
            $orderProduct->save();
        } else {
            $orderProduct = \Yii::CreateObject(OrderProduct::className());
            $orderProduct->attributes = [
                'order_id' => $this->id,
                'product_id' => $product->id,
                'count' => 1
            ];
            $orderProduct->save();
        }
        return true;
    }

    public function getTotalPrice() {
        $totalPrice = 0;
        foreach ( $this->orderProducts as $orderProduct ) { 
            $totalPrice += $orderProduct->totalPrice;
        }
        return $totalPrice;
    }

    public function getTotalCount() {
        $totalCount = 0;
        foreach ( $this->orderProducts as $orderProduct ) { 
            $totalCount += $orderProduct->count;
        }
        return $totalCount;
    }

}
