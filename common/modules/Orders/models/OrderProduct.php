<?php

namespace common\modules\Orders\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%orders}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $count
 * @property integer $created_at
 * @property integer $updated_at
 */
class OrderProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders_products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'created_at', 'updated_at', 'count'], 'integer'],
            ['count', 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            /*'id' => Yii::t('app\Orders', 'ID'),
            'user_id' => Yii::t('app\Orders', 'User ID'),
            'status' => Yii::t('app\Orders', 'Status'),
            'comments' => Yii::t('app\Orders', 'Comments'),
            'city' => Yii::t('app\Orders', 'City'),
            'address' => Yii::t('app\Orders', 'Address'),
            'postal_code' => Yii::t('app\Orders', 'Postal Code'),
            'created_at' => Yii::t('app\Orders', 'Created At'),
            'updated_at' => Yii::t('app\Orders', 'Updated At'),*/
        ];
    }

    public function getProduct() {
        return $this->hasOne(\common\modules\Products\models\Product::className(), ['id' => 'product_id']);
    }

    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function getTotalPrice() { 
        return $this->product->price * $this->count;
    }

    public function addProductToCart( $product ) { 

    }

}
