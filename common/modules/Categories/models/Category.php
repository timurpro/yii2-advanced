<?php

namespace common\modules\Categories\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\modules\Categories\Module;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $image_id
 * @property string $description
 * @property integer $parent_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['parent_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'image_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('model', 'ID'),
            'name' => Module::t('model', 'Name'),
            'image_id' => Module::t('model', 'Image ID'),
            'description' => Module::t('model', 'Description'),
            'parent_id' => Module::t('model', 'Parent ID'),
            'created_at' => Module::t('model', 'Created at'),
            'updated_at' => Module::t('model', 'Updated at'),
        ];
    }

    public function getProducts() {
        return $this->hasMany(\common\modules\Products\models\Product::className(), ['id' => 'product_id'])->viaTable('categories_products', ['category_id' => 'id']);
    }

    public function getSubCategories() {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    public function getParentCategory() {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    public function getParentCategories() {
        $result = [$category = $this];
        while ( $category = $category->getParentCategory()->one() ) {
            array_push($result, $category);
        }
        return $result;
    }

    /**
     * Возвращает имя категории. При $withPath вернет полный путь включая 
     * родительские категории через "/", например, "Категория 1 / Категория 2 / Категория 3"
     *
     * @param $withPath Boolean Вернуть полный путь до категории
     * @return string имя или полный путь
     */
    public function getName($withPath = false) {
        if ( !$withPath ) {
            return $this->name;
        }
        $names = array_map(function ($category) {
            return $category->name;
        }, array_reverse($this->getParentCategories()));
        return join(' / ', $names);
    }

    public function setName($name) {
        $this->name;
        return $this;
    }

}
