<?php

namespace common\modules\Categories\widgets;

use Yii;
use common\modules\Categories\models\Category;
use yii\helpers\Url;

class CategoriesSideBar extends \yii\base\Widget
{
    private static $parentsTree = [];
    private static $selectedCategory = null;

    /**
     * Функция устанавливает выбранну категорию в виджете
     * Если заполнен параметр $category, то при отображении виджета будут отображены 
     * все родители этой категории, а также ее дочерние категории первого уровня
     * Если параметр $category не передан, то виджет отработает в штатном режиме - 
     * отобразит только корневые категории
     * Использование:
     *
     * use common\modules\Categories\models\Category;
     * 
     * $category = Category::findOne(1)->one();
     * CategoriesSideBar::setSelectedCategory($category);
     *
     * @param $category common\modules\Categories\models\Category|null Объект категории
     * @return void
     * @author Рахимжанов Тимур <t@timur.pro>
     */
    public static function setSelectedCategory($category = null) {
        if ( !empty($category) && $category instanceof Category ) {
            CategoriesSideBar::$parentsTree = [$category];
            if ( $category->parent_id != null ) {
                CategoriesSideBar::$parentsTree = $category->getParentCategories();
            }
            CategoriesSideBar::$selectedCategory = $category;
        }
    }

    /**
     * @todo: Написать описание функции
     **/
    private function getCategoriesTree($parents) {
        $parent = array_pop($parents);
        $nextParent = end($parents);
        $categories = $parent->getSubCategories()->all();
        $itemsList = [];
        foreach ( $categories as $item ) {
            $itemArray = [
                'label' => $item->name,
                'url' => Url::to(['/categories/default/index', 'id' => $item->id])
            ];
            if ( $nextParent && $nextParent->id == $item->id ) {
                $itemArray['items'] = $this->getCategoriesTree($parents);
            }
            $itemsList[] = $itemArray;
        }
        return $itemsList;
    }

    public function init() {
        $categories = Category::find()->where(['parent_id' => null])->all();
        $itemsList = [[
            'label' => 'Категории',
            'url' => Url::to(['/categories/default/index']),
        ]];
        $rootCategory = !empty(CategoriesSideBar::$selectedCategory) ? end(CategoriesSideBar::$parentsTree) : null;
        foreach ( $categories as $item ) {
            $itemArray = [
                'label' => $item->name,
                'url' => Url::to(['/categories/default/index', 'id' => $item->id]),
            ];
            if ( !empty($rootCategory) && $rootCategory->id == $item->id ) {
                $itemArray['items'] = $this->getCategoriesTree(CategoriesSideBar::$parentsTree);
            }
            $itemsList[] = $itemArray;
        }
        if ( !empty($itemsList) ) {
            echo \yii\widgets\Menu::widget([
                'items' => $itemsList,
                'options' => [
                    'class' => 'menu-list',
                ],
                'linkTemplate' => '<a href="{url}"><div>{label}</div></a>',
            ]);
        }
    }
}

?>