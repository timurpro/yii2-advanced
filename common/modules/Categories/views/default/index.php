<?php

/* @var $this yii\web\View */

$this->title = $title;

use yii\widgets\ListView;
use common\modules\Products\widgets\ProductsListWidget;
?>

<div class="Categories-default-index">
    <div class="Category-name">
        <?= $title ?>
    </div>
    <div class="body-content">
        <div class="category_list">
            <?= 
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_category',
                    'layout' => "{items}\n{pager}"
                ]);
            ?>
        </div>
        <?php if ( !empty($productsDataProvider) ) { ?>
            <div class="product_list">
                <?= 
                    ProductsListWidget::widget([
                        'dataProvider' => $productsDataProvider,
                    ]);            
                ?>
            </div>
        <?php } ?>
    </div>
</div>
