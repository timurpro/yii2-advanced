<?php
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Button;
use yii\web\Controller;
use yii\widgets\ActiveForm;
use common\modules\Categories\Module;

?>
<div class="Categories-default-edit">
    <section id="page-title">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="mainTitle"><?= Module::t('module', 'Category'); ?>: <?= !isset($model->name) ? Module::t('module', 'New category') : $model->name;  ?></h1>
                <span class="mainDescription"><?= !isset($model->name) ? Module::t('module', 'Category create') : Module::t('module', 'Category edit'); ?></small></span>
            </div>
        </div>
    </section>
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin([
                    'id' => 'CategoryEditForm',
                    'method' => 'post',
                    'action' => ['save', 'id' => $model->id],
                ]); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'description') ?>
                <?= $form->field($model, 'parent_id') ?>
                <?=
                    Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']);
                ?>
                <?= Html::a(Module::t('module', 'Cancel'), isset($module->id) ? ['view', 'id' => $model->id] : ['index'], ['class' => 'btn']); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
