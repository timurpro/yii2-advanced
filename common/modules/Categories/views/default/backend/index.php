<?php

/* @var $this yii\web\View */

$this->title = $title;

use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use common\modules\Categories\Module;

?>

<div class="Categories-default-index">
    <section id="page-title">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="mainTitle"><?= Module::t('module', 'Categories list'); ?></h1>
                <span class="mainDescription"><?= Module::t('module', 'Here you can manage your categories'); ?></small></span>
            </div>
        </div>
    </section>
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <div class="col-md-12">
                <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            'id',
                            [
                                'attribute' => 'name',
                                'value' => function($model){
                                    return Html::a(Html::encode($model->name), Url::to(['view', 'id' => $model->id]));
                                },
                                'format' => 'raw',
                            ],
    			            [
        			            'attribute'=>'parentCategory.name',
    			                'label'=>'Родительская категория',
    			                'format'=>'text',
    			            ],
                            'created_at:datetime',
                            'updated_at:datetime'
                        ],
                        'layout' => "{items}\n{pager}",
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover',
                        ]

                    ]);
                ?>
            </div>
        </div>
        <?= Html::a('New', ['edit', 'id' => '0'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>

