<?php

/* @var $this yii\web\View */



use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Button;
use yii\web\Controller;
use common\modules\Categories\Module;

?>

<div class="Categories-default-view">
    <section id="page-title">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="mainTitle"><?= Module::t('module', 'Category'); ?>: <?= $model->name ?></h1>
                <span class="mainDescription"><?= Module::t('module', 'Category details'); ?></small></span>
            </div>
        </div>
    </section>
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <div class="col-md-12">
                <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'name',
                            'description:html',
                    	    'parentCategory.name',
                            'created_at:datetime',
                            'updated_at:datetime'
                        ],
                    ]);
                ?>
            </div>
        </div>
        <?= Html::a('Edit', ['edit', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
        <?= Html::a(Module::t('module', 'Back to list'), ['index'], ['class' => 'btn']); ?>
    </div>
</div>
