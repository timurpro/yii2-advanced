<?php
use yii\helpers\Url;
?>
<div class="col-sm-4 col-md-4 col-xs-4">
    <a href="<?= Url::to(['/categories/default/index', 'id' => $model->id]); ?>">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="category_name"><?= $model->name ?></div>
        </div>
    </div>
    </a>
</div>