<?php

return [
    'Category' => 'Категория',
    'Categories' => 'Категории',
    'Category details' => 'Информация по категории',
    'Categories list' => 'Список Категорий',
    'Here you can manage your categories' => 'На данной странице вы можете управлять своими категориями',
    'Edit' => 'Править',
    'Back to list' => 'К списку',
    'Cancel' => 'Отмена',
    'Category create' => 'Создание категории',
    'Category edit' => 'Редактирование категории',
    'New category' => 'Новая категория'
];

?>