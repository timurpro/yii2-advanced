<?php

return [
   'ID' => 'Ид',
   'Name' => 'Название',
   'Image ID' => 'Изображение',
   'Description' => 'Описание',
   'Parent ID' => 'Родительская категория',
   'Created at' => 'Дата создания',
   'Updated at' => 'Дата изменения',
];

?>