<?php

namespace common\modules\Categories;

use yii\helpers\Url;

/**
 * Categories module definition class
 */
class Module extends \yii\base\Module
{
    use \common\traits\I18NModuleTrait;
    private static $I18N_NAMESPACE = 'common/modules/Categories';
    private static $I18N_GROUPS = [ 'module', 'model' ];
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\Categories\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public static function menuElements() {
        return [
            'label' => self::t('module', 'Categories'),
            'icon' => '',
            'url' => Url::to(['/categories/default/index']),
        ];
    }
}
