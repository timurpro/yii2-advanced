<?php

namespace common\modules\Categories\controllers\backend;

use yii\web\Controller;
use common\modules\Categories\models\Category;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `Categories` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function Init() {
        parent::Init();
        $this->setViewPath($this->getViewPath().'/backend');
    }

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Category::find()
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'title' => 'Категории'
        ]);
    }

    public function actionView($id) {
        return $this->render('view',[
            'model'=>Category::findOne($id),
        ]);
    }

    public function actionEdit($id = null) {
        if ( !($model = Category::findOne($id)) ) {
            $model = new Category();
        } 
        return $this->render('edit',[
            'model' => $model,
        ]);
    }

    public function actionSave($id = null) {
        if ( (!$model = Category::findOne($id)) ) {
            $model = new Category();
        }
        if ( $model->load(\Yii::$app->request->post()) && $model->save() ) {
            \Yii::$app->getSession()->setFlash('Success','Save Ok');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            \Yii::$app->getSession()->setFlash('Error','Save Error');
            return $this->render('edit', ['model' => $model]);
        }
    }      
}
