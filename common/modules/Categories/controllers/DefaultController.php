<?php

namespace common\modules\Categories\controllers;

use yii\web\Controller;
use common\modules\Categories\models\Category;
use common\modules\Categories\widgets\CategoriesSideBar as CategoriesWidget;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `Categories` module
 */
class DefaultController extends Controller
{
    /**
     * Отображает список корневых категорий если не передан id категории
     * Иначе отображается список подкатегорий и список товаров в этой категории
     * @return string
     */
    public function actionIndex($id = null)
    {
        if ( $category = Category::findOne($id) ) {
            CategoriesWidget::setSelectedCategory($category);
            return $this->render('index', [
                'category' => $category,
                'dataProvider' => new ActiveDataProvider([
                    'query' => $category->getSubCategories()
                ]),
                'productsDataProvider' => new ActiveDataProvider([
                    'query' => $category->getProducts()
                ]),
                'title' => $category->getName(true)
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => Category::find()->where(['parent_id' => null])
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'title' => 'Категории'
        ]);
    }
}
