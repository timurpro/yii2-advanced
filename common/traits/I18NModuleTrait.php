<?php

namespace common\traits;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Трейт для реализации локализации модуля
 * ВНИМАНИЕ: Объявление переменных $I18N_NAMESPACE и $I18N_GROUPS в классе модуля - ОБЯЗАТЕЛЬНО:
 *
 * use \common\traits\I18NModuleTrait;
 * private static $I18N_NAMESPACE = 'common/modules/Products';
 * private static $I18N_GROUPS = [ 'model', 'card', 'module' ];
 *
 **/
trait I18NModuleTrait {

    /** 
     * Namespace для файлов переводов. 
     * Best-practice: хранить переводы модуля лучше в директории модуля, 
     * поэтому значение константы должно быть равно пути до директории модуля.
     * Например, для модуля Products:
     * private static $I18N_NAMESPACE = 'common/modules/Products';
     */

    /**
     * Досупные группы переводов
     * Должен содержать имена файлов в директории messages/[language], например:
     * private static $I18N_GROUPS = [ 'model', 'card', 'module' ];
     */

    /**
     * Функция возвращает массив с маппингом групп с файлами
     */
    private static function getI18nFileMap() {
        $groups = array_map(function ($key) {
            return self::$I18N_NAMESPACE . '/' . $key;
        }, self::$I18N_GROUPS);
        return array_combine($groups, array_map(function($i) {
            return $i . '.php';
        }, self::$I18N_GROUPS));
    }

    /**
     * Функция регистрирует категории для переводов.
     */
    public static function registerTranslations()
    {
        if ( empty(Yii::$app->i18n->translations[self::$I18N_NAMESPACE.'/*']) ) {
            Yii::$app->i18n->translations[self::$I18N_NAMESPACE.'/*'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => '@'.self::$I18N_NAMESPACE.'/messages',
                'fileMap' => self::getI18nFileMap(),
            ];
        }
    }

    /**
     * Функция возварщает перевод сообщения $message из категории $category
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        self::registerTranslations(); 
        return Yii::t(self::$I18N_NAMESPACE . '/' . $category, $message, $params, $language);
    }

}


?>