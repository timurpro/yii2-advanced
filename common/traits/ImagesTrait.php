<?php

namespace common\traits;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Трейт для работы с изображениями в модели
 */
trait ImagesTrait {

    private $images_relationship = 'images';
    private $images_ids = [];

    /**
     * Функция возвращает объект Image
     * @return \common\models\Image
     */
    public static function getImageObject($params = []) 
    {   
        return \Yii::createObject(ArrayHelper::merge([
            'class' => 'common\models\Image',
            'attributes' => [
                'parent_class' => self::ClassName()
            ]
        ], $params));
    }

    /**
     * Функция загружает и сохраняет в базе изображения, переданные в массиве.
     * @param $images Array Массив изображений. Все элементы массива
     *                      должны быть объектами класса \yii\web\UploadedFile
     * @return $this 
     */
    public function setImages($images) 
    {
        // Если в качестве параметра передан не массив, 
        if ( !empty($images) && !is_array($images) ) {
            // то преобразуем его в массив из одного элемента
            $images = [$images];
        }
        // Загружаем изображения из массива
        foreach ( $images as $image ) {
            $this->setImage($image);
        }
        return $this;
    }

    /**
     * Функция загружает и сохраняет изображение, переданное в параметре $image
     * @param $image \yii\web\UploadedFile Объект загруженного изображения
     * @return $this
     */
    public function setImage($image) 
    {
        if ( empty($image) ) {
            return $this;
        }
        if ( !($image instanceof yii\web\UploadedFile) ) {
            throw new \yii\base\InvalidParamException('Image should be an instance of \yii\web\UploadedFile');
        }
        $imageObject = self::getImageObject(['attributes' => ['imageFile' => $image]]);
        $imageObject->upload();
        $imageObject->save();
        $this->link($this->images_relationship, $imageObject);
        return $this;
    }

    /**
     * Связь базового объекта с объектом изображения
     */
    public function getImages() {
        return $this->hasMany(\common\models\Image::ClassName(), ['parent_id' => 'id'])->where(['parent_class' => self::ClassName()]);
    }
}


?>