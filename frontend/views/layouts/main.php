<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="main-container">
        <div class="header">
            <div class="logo">
                Logo
            </div>
            <div class="header-center">
                Center
            </div>
            <div class="cart">
                <?= \common\modules\Orders\widgets\Cart::Widget(); ?>
            </div>
        </div>
        <div class="menu">
            <?= \common\modules\Menu\widgets\MainMenu::Widget(); ?>
        </div>
        <div class="body">
            <div class="sidebar">
                <div class="categories">
                    <?= \common\modules\Categories\widgets\CategoriesSideBar::Widget(); ?>
                </div>
            </div>
            <div class="content">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
