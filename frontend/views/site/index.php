<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use common\modules\Products\widgets\ProductsListWidget;
?>
<div class="site-index">

    <div class="body-content">
        <div class="products_list">
            <?= 
                ProductsListWidget::widget([
                    'dataProvider' => $dataProvider,
                ]);
            ?>
        </div>
    </div>

</div>
