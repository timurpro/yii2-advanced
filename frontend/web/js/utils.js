function addProductToCart( id ) {
    if ( !id ) { return false; }
    $.get({
        url: '/orders/products/add',
        data: {
            id: id
        },
        success: function(response) {
            $('.header > .cart').html(response);
        }
    });
}