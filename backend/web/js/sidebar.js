$(document).ready(function() {
    /**
     * Обработчик события кнопки показа/скрытия боковой панели меню
     */
    $('a.sidebar-toggler').on('click', function(event) {
        event.preventDefault();
        $('div#app').toggleClass('app-sidebar-closed');
    });
});