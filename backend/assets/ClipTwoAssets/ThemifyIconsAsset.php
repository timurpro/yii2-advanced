<?php

namespace backend\assets\ClipTwoAssets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class ThemifyIconsAsset extends AssetBundle
{
    public $sourcePath = '@bower/themify-icons/';
    public $css = [
        'css/themify-icons.css',
    ];
    public $js = [
    ];
    public $depends = [
    ];
}
