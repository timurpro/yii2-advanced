<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class ClipTwoAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/clip_two_theme/styles.css',
        'css/clip_two_theme/plugins.css'
    ];
    public $js = [
        'js/sidebar.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\ClipTwoAssets\ThemifyIconsAsset'
    ];

    public function init() {
        parent::init();
        $theme = (isset(\Yii::$app->params['theme']) ? \Yii::$app->params['theme'] : 'theme-6');
        $this->css[] = 'css/clip_two_theme/themes/' . $theme . '.css';
    }
}
