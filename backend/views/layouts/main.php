<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\ClipTwoAppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

ClipTwoAppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class='js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths'>
<?php $this->beginBody() ?>

    <div id="app" class="app-navbar-fixed app-sidebar-fixed">
        <!-- Start: Side Bar --> 
        <div class="sidebar app-aside" id="sidebar">
            <div class="sidebar-container perfect-scrollbar">
                <nav>
                    <div class="navbar-title">
                        <span>Main Navigation</span>
                    </div>
                    <?php 
                        if ( $this->beginCache('sidebar-menu', ['duration' => 1]) ) {
                            $menuItems = [];
                            foreach ( \Yii::$app->modules as $module => $moduleClass ) {
                                if ( in_array($module, ['debug', 'gii']) ) {
                                    continue;
                                }
                                $active = false;
                                if ( is_array($moduleClass) ) {
                                    $className = $moduleClass['class'];
                                } else {
                                    $className = $moduleClass::className();
                                    $active = true;
                                }

                                if ( method_exists($className, 'menuElements') ) {
                                    $moduleMenu = $className::menuElements();
                                    $icon = !empty($moduleMenu['icon']) ? $moduleMenu['icon'] : 'ti-layout-sidebar-none';
                                    $label = <<<HTML
                                            <div class="item-content">
                                                <div class="item-media">
                                                    <i class="{$icon}"></i>
                                                </div>
                                                <div class="item-inner">
                                                    <span class="title">{$moduleMenu['label']}</span>
                                                </div>
                                            </div>
HTML;
                                    $menuItems[] = [
                                       'label' => $label,  
                                        'encode' => false,
                                        'url' => ['/'.$module.'/default'],
                                        'active' => $active
                                    ];
                                }
                            }
                    ?>
                            <?= Nav::widget([
                                'options' => ['class' => 'main-navigation-menu'],
                                'items' => $menuItems
                            ]); ?>
                    <?php
                            $this->endCache(); 
                        } 
                    ?>
                </nav>
            </div>
        </div>
        <!-- End: Side Bar --> 
        <div class="app-content">
            <!-- Start: Header Bar --> 
            <header class="navbar navbar-default navbar-static-top">
                <div class="navbar-header">
                    <a href="#" class="sidebar-mobile-toggler pull-left hidden-md hidden-lg" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
                        <i class="ti-align-justify"></i>
                    </a>
                    <a class="navbar-brand" href="#">
                        Shop Yii2
                    </a>
                    <a href="#" class="sidebar-toggler pull-right visible-md visible-lg" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
                        <i class="ti-align-justify"></i>
                    </a>
                    <a class="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="ti-view-grid"></i>
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-right">
                        <?php
                            if (Yii::$app->user->isGuest) {
                                echo "<li>" . Html::a('Login', ['/site/login']);
                            } else {
                                echo '<li>'
                                    . Html::beginForm(['/site/logout'], 'post')
                                    . Html::submitButton(
                                        'Logout (' . Yii::$app->user->identity->username . ')',
                                        ['class' => 'btn btn-link logout']
                                    )
                                    . Html::endForm()
                                    . '</li>';
                            }
                        ?>
                    </ul>
                </div>
            </header>
            <!-- End: Header Bar --> 
            <!-- Start: Content --> 
            <div class="main-content" >
                <div class="wrap-content container" id="container">
                    <?= $content ?>
                </div>
            </div>
            <!-- End: Content --> 
        </div>
        <!-- start: FOOTER -->
        <footer>
            <div class="footer-inner">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>
        <!-- end: FOOTER -->
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
